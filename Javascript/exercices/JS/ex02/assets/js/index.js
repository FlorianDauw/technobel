let bieres = {};
let encore = true;
while(encore){

    afficherBieres();
    let choix = parseInt(prompt("1) Ajouter une bière\n2) Modifier les stocks\n3) Supprimer une bière\n4) Arrêter"));
    encore = choix != 4;
    
    switch (choix) {
        case 1:
            ajouterBiere();
            break;
        
        case 2:
            modifierStocks();
            break;

        case 3:
            supprimerBiere();
            break;

        case 4:
            alert("A bientôt !");
            break;

        default:
            alert("Entrez un choix valide svp!");
            break;
    }
}

function afficherBieres(){
    console.clear();
    console.table(bieres);
}

function ajouterBiere() {
    let biere = prompt("Quelle bière souhaitez-vous ajouter ?").toUpperCase();
    let quantite = parseInt(prompt(`Combien de ${biere} souhaitez-vous ajouter ?`));
    bieres[biere] = quantite;
}

function modifierStocks() {
    let biere = prompt("Quel stock bière souhaitez-vous modifier ?").toUpperCase();
    let quantite = parseInt(prompt(`Combien de ${biere} voulez-vous ?`));
    if(bieres.hasOwnProperty(biere))
        bieres[biere] = quantite;
    else{
        let ajout = prompt("Cette bière n'est pas encore stockée, souhaitez-vous l'ajouter ?\n1) Oui\n2) Non");
        if(ajout == 1)
            bieres[biere] = quantite;
    }
}

function supprimerBiere() {
    let biere = prompt("Quelle bière souhaitez-vous supprimer ?").toUpperCase();
    delete bieres[biere];
}