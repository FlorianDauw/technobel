let miniatures_list = document.querySelectorAll('#miniatures li img');
let bigPic = document.getElementById("big-picture");
for(let item of miniatures_list) {
    item.addEventListener('click', e => {
        let src = e.target.getAttribute('src');
        bigPic.setAttribute('src', src);
    });
}