let context = [
	{ id: 1, name: 'Sushi Thon', image: 'sushi_thon.jpg', price: 4.5, category: 'Sushi' },	
	{ id: 2, name: 'Sushi Saumon', image: 'sushi_saumon.jpg', price: 4.2, category: 'Sushi' },
	{ id: 3, name: 'Sushi Daurade', image: 'sushi_daurade.jpg', price: 4.5, category: 'Sushi' },
	{ id: 4, name: 'Sushi Omelette', image: 'sushi_omelette.jpg', price: 3, category: 'Sushi' },
	{ id: 5, name: 'Sushi Crevette', image: 'sushi_crevette.jpg', price: 4, category: 'Sushi' },
	{ id: 6, name: 'Maki Thon', image: 'maki_thon.jpg', price: 4, category: 'Maki' },
	{ id: 7, name: 'Maki Saumon', image: 'maki_saumon.jpg', price: 3.5, category: 'Maki' },
	{ id: 8, name: 'Maki Concombre', image: 'maki_concombre.jpg', price: 3, category: 'Maki' },
	{ id: 9, name: 'Temaki Saumon', image: 'temaki_saumon.jpg', price: 6, category: 'Temaki' },
	{ id: 10, name: 'Chirashi Saumon', image: 'chirashi_saumon.jpg', price: 15, category: 'Chirashi' },
	{ id: 11, name: 'Chirashi Saumon Thon', image: 'chirashi_saumon_thon.jpg', price: 16, category: 'Chirashi' },
	{ id: 12, name: 'Chirashi Thon', image: 'chirashi_thon.jpg', price: 15, category: 'Chirashi' },
	{ id: 13, name: 'Tempura Crevette ', image: 'tempura_crevette.jpg', price: 6, category: 'Tempura' },
	{ id: 14, name: 'Tempura Legumes ', image: 'tempura_legumes.jpg', price: 4, category: 'Tempura' }
];

let cart = [];

let quantity = 0;

$(function(){
	let headerH = parseInt($('header').css('height'));
	$(window).on('scroll', function(event) {
	    let scrollValue = $(window).scrollTop();
	    if (scrollValue > headerH) {
	        $('.navbar').addClass('fixed-top');
	    }
	    else{
	    	$('.navbar').removeClass('fixed-top');
	    }
	});

	let articles = $('#main .row');
	let template = 
	`
		<div class="col-md-4 col-lg-3 mb-3 p-5">
			<div class="card">
				<img class="card-img-top __category__" src="__image__" alt="default image">
				<div class="card-body position-relative">
					<div class="price">__price__€</div>
						<p>__name__</p>
				</div>
			</div>
		</div>
	`;

	for(let art of context){
		let card = template
			.replace('__image__', 'images/' + art.image)
			.replace('__name__', art.name)
			.replace('__price__', art.price)
			.replace('__category__', art.category)

		articles.append(card);
	}
	articles.on('click','.card', function() {
		$("#confirm-modal").modal();
	});
	$('#confirm-add').on('click', function() {
		$('.toast').toast('show');
		$("#quantity").html(++quantity);
	});
});




let button = document.getElementById('btn_trier');
let select = document.getElementById('select_category');
let produits = document.getElementsByClassName('card-img-top');



button.addEventListener('click', e => {
	console.log(select.value);
	console.table(produits);
	for(let item of produits){
		let card = item.parentElement;
		let div = card.parentElement;
		if(select.value === "All") {
			card.classList.remove('hidden');
			div.classList.add('col-md-4', 'col-lg-3', 'mb-3', 'p-5');
		}
		else{
			if(card.classList.contains('hidden')){
				card.classList.remove('hidden');
				div.classList.add('col-md-4', 'col-lg-3', 'mb-3', 'p-5');
			}
			if(!item.classList.contains(select.value)){
				card.classList.add('hidden');
				div.classList.remove('col-md-4', 'col-lg-3', 'mb-3', 'p-5');
			}
		}
	}
})