let ul = document.getElementById('toDoList');
let taskNameInput = document.querySelector("#taskName");
let btnAdd = document.querySelector("#add");

//Soit on déclare la fonction dans une variable et on la passe en param de l'eventListener, soit fonction anonyme directement en param

let supprimer = event => {
    //event.target correspond au "sender"
    let object = event.target;
    console.log(object);

    //supprimer li
    let li = object.parentNode;
    ul.removeChild(li);

    console.log(li);
    
}

//2params : 1 = nom de l'event, 2 = 
btnAdd.addEventListener('click', (event) => {

    let item = document.createElement('li');

    item.innerText = taskNameInput.value;

    let btn = document.createElement('button');

    btn.innerText = '-';

    btn.addEventListener('click', supprimer);

    item.appendChild(btn);

    ul.appendChild(item);

    taskNameInput.value = null;
});

taskNameInput.addEventListener('keypress', event => {
    if(event.key == 'Enter') {
        let item = document.createElement('li');

        item.innerText = taskNameInput.value;
    
        let btn = document.createElement('button');
    
        btn.innerText = '-';
    
        btn.addEventListener('click', supprimer);
    
        item.appendChild(btn);
    
        ul.appendChild(item);
    
        taskNameInput.value = null;
    }
    
})


