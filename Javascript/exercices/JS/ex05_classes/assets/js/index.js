// let button_up = document.getElementById('btn-up');
// let button_down = document.getElementById('btn-down');
// let content = document.getElementById('content');


// button_up.addEventListener('click', e => {
//     button_up.classList.replace('visible', 'hidden');
//     button_down.classList.replace('hidden', 'visible');
//     content.classList.replace('visible', 'hidden');
// });

// button_down.addEventListener('click', e => {
//     button_up.classList.replace('hidden', 'visible');
//     button_down.classList.replace('visible', 'hidden');
//     content.classList.replace('hidden', 'visible');
// });

let caret = document.getElementById('btn');
let p = document.getElementById("content");

caret.addEventListener('click', e => {
    p.classList.toggle('hidden');
    caret.classList.toggle('fa-caret-up');
    caret.classList.toggle('fa-caret-down');
});