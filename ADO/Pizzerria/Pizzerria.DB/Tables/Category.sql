﻿CREATE TABLE [dbo].[Category]
(
	[Id] INT IDENTITY(1,1) PRIMARY KEY,
	[Category] NVARCHAR(50) NOT NULL
)
