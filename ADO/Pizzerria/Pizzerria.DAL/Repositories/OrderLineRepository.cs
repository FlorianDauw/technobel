﻿using Pizzerria.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox.ADO.Utils;

namespace Pizzerria.DAL.Repositories
{
    public class OrderLineRepository : BaseRepository<OrderLine>
    {
        public OrderLineRepository(string connectionString, string providerName) : base(connectionString, providerName)
        {
        }
    }
}
