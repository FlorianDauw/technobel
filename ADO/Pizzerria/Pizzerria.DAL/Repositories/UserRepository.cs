﻿using Pizzerria.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox.ADO.Utils;

namespace Pizzerria.DAL.Repositories
{
    public class UserRepository : BaseRepository<User>
    {
        public UserRepository(string connectionString, string providerName) : base(connectionString, providerName)
        {
        }
        public IEnumerable<User> GetUsersByRole(string role)
        {
            using (IDbConnection conn = GetConnection())
            {
                conn.Open();
                string query = "SELECT * FROM User WHERE Role = @p1";
                Dictionary<string, object> dico = new Dictionary<string, object>
                {
                    { "@p1", role }
                };
                IDbCommand cmd = CreateCommand(conn, query, dico);
                IDataReader r = cmd.ExecuteReader();
                while (r.Read())
                {
                    yield return ReaderToEntityMapper(r);
                }
            }
        }
    }
}
