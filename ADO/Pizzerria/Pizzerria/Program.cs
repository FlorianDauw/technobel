﻿using Pizzerria.DAL.Entities;
using Pizzerria.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pizzerria
{
    class Program
    {
        static string ServerName = @"5218\SQLSERVER";
        static string DbName = "Pizzerria";
        static string Username = "sa";
        static string Password = "test1234";
        static void Main(string[] args)
        {
            string connectionString
                = $@"Data Source={ServerName};Initial Catalog={DbName};User ID={Username};Pwd={Password}";

            #region Repositories
            CategoryRepository categoryRepository = new CategoryRepository(
                connectionString,
                "System.Data.SqlClient"
            );

            OrderLineRepository orderLineRepository = new OrderLineRepository(
                connectionString,
                "System.Data.SqlClient"
            );

            OrderRepository orderRepository = new OrderRepository(
                connectionString,
                "System.Data.SqlClient"
            );

            ProductRepository productRepository = new ProductRepository(
                connectionString,
                "System.Data.SqlClient"
            );

            UserRepository userRepository = new UserRepository(
                connectionString,
                "System.Data.SqlClient"
            ); 
            #endregion

            //IEnumerable<User> users = userRepository.Get();
            //foreach (User u in users)
            //{
            //    Console.WriteLine(u.FirstName);
            //}
        }
    }
}