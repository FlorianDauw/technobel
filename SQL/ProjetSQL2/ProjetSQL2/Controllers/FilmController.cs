﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ProjetSQL2.Controllers
{
    public ActionResult Index()
    {
        FilmService serv = new FilmService();
        List<Film> model = serv.GetAll();
        return View(model);
    }

    public ActionResult Details(int id)
    {
        StudentService serv = new StudentService();
        Student model = serv.Get(id);
        return View(model);
    }
}
