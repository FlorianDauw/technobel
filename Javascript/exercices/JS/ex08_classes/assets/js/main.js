// let spans = document.getElementsByTagName('span');

// console.log(spans[0]);

// let pop_up = document.createElement('span');
// pop_up.innerText = "Hey !";
// pop_up.classList.add('pop_up');

// for(let item of spans){
    
//     item.addEventListener('mouseover', e => {
//         let current_span = e.target;
//         current_span.appendChild(pop_up);
        
//         // alert("test");


//     });
// }

let popList = document.getElementsByClassName('pop');
for(let pop of popList) {
    let s = document.createElement('span');

    //récupère l'attribut "..." d'un élément
    let text = pop.getAttribute('data-text');

    //modifier l'attribut "..." d'un élément :
    //pop.setAttribute("...", valeur);
    
    s.innerText = text;
    s.classList.add("popup");
    pop.appendChild(s);

    pop.addEventListener('mouseover', e => {
        let current = e.target;
        // let children = current.childNodes;
        let span = current.childNodes[1];
        span.classList.add('show');
        
    });

    pop.addEventListener('mouseout', e => {
        let current = e.target;
        let span = current.childNodes[1];
        span.classList.remove('show');
    });
}