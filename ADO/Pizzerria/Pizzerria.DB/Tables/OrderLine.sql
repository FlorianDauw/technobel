﻿CREATE TABLE [dbo].[OrderLine]
(
	[Id] INT IDENTITY(1,1) PRIMARY KEY,
	[Quantity] INT NOT NULL,
	[Price] MONEY NOT NULL,
	[ProductId] INT FOREIGN KEY REFERENCES Product(Id) ON DELETE SET NULL,
	[OrderId] INT FOREIGN KEY REFERENCES [Order](Id) ON DELETE CASCADE
)
