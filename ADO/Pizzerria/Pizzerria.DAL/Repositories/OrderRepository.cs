﻿using Pizzerria.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox.ADO.Utils;

namespace Pizzerria.DAL.Repositories
{
    public class OrderRepository : BaseRepository<Order>
    {
        public OrderRepository(string connectionString, string providerName) : base(connectionString, providerName)
        {
        }
        public IEnumerable<Order> GetOrdersByStatus(string status)
        {
            using (IDbConnection conn = GetConnection())
            {
                conn.Open();
                string query = "SELECT * FROM Order WHERE Status = @p1";
                Dictionary<string, object> dico = new Dictionary<string, object>
                {
                    { "@p1", status }
                };
                IDbCommand cmd = CreateCommand(conn, query, dico);
                IDataReader r = cmd.ExecuteReader();
                while (r.Read())
                {
                    yield return ReaderToEntityMapper(r);
                }
            }
        }
    }
}
