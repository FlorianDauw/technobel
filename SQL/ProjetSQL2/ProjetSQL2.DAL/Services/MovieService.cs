﻿using ProjetSQL2.DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace ProjetSQL2.DAL.Services
{
    public class MovieService
    {
        private string connectionString = @"Data Source=5218\SQLSERVER;Initial Catalog=Movie;UID=sa;Pwd=test1234";
        public List<Movie> GetAll()
        {
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();
            SqlCommand cmd = connection.CreateCommand();
            cmd.CommandText = "SELECT * FROM movies";
            SqlDataReader r = cmd.ExecuteReader();
            List<Movie> result = new List<Movie>();
            while (r.Read())
            {
                result.Add(new Movie
                {
                    Id = (int)r["film_id"],
                    Title = (string)r["film_title"]
                });
            }
            connection.Close();
            return result;
        }

        //public Student Get(int id)
        //{
        //    SqlConnection connection = new SqlConnection(connectionString);
        //    connection.Open();
        //    SqlCommand cmd = connection.CreateCommand();
        //    cmd.CommandText = "SELECT * FROM student WHERE student_id = " + id;
        //    SqlDataReader r = cmd.ExecuteReader();
        //    Student s = null;
        //    if (r.Read())
        //    {
        //        s = new Student
        //        {
        //            Id = (int)r["student_id"],
        //            LastName = (string)r["last_name"],
        //            FirstName = (string)r["first_name"],
        //            sectionId = (int)r["section_id"]
        //        };
        //    }
        //    connection.Close();
        //    return s;
        //}

        //public int Insert(Student s)
        //{

        //}
    }
}
