let maVariable = 42;
const MA_CONSTANTE = 50;

// à éviter
// var maVariable2 = 43;

/*
Types :

-Number
-String
-Function
-Object
-List / Array
-Dictionnary
-Booléen
*/

// type number
let monNombre = 45.56;

//type string
let maChaine = "ma chaine";
let maChaine2 = 'ma chaine 2';
let maChaine3 = `ma chaine 3`;

//type array (liste)
let tableau = [55, 'string', []];

//type dico (tableau associatif)
let dictionnaire = {
    maCle : 'maValeur',
    maCle : 45
    //Les clés sont toujours des chaînes de caractère /!\
};

//type function
let maFonction = function() {};
let maFonction2 = () => {};
function maFonction3() {
    //corps de la fonction
}

//type booléen
let monBooleen = true;

//------------------------------------------------------------------------------------------------------------------------------

//output
console.log(monNombre);
console.table(tableau);

//input
let age = prompt("Quel est votre âge ?");

//------------------------------------------------------------------------------------------------------------------------------

//concaténation
let maChaine4 = `mon nombre : ${monNombre}`;
let maChaine5 = maChaine + " " + maChaine2;

//------------------------------------------------------------------------------------------------------------------------------

//conversion
age = parseInt(age);

//------------------------------------------------------------------------------------------------------------------------------

/* 
Raccourcis

1) log = console.log();
2) 

*/

console.log(age);
