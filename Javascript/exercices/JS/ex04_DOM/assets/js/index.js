let firstName = document.querySelector("#firstName");
let lastName = document.querySelector("#lastName");
let birthDate = document.querySelector("#birthDate");
let btnAdd = document.querySelector("#btnAdd");

let tbody = document.querySelector("#bodyUsersTable");

btnAdd.addEventListener('click', event => {

    let tr = document.createElement('tr');

    let tdFirstName = document.createElement('td');
    tdFirstName.innerText = firstName.value;

    let tdLastName = document.createElement('td');
    tdLastName.innerText = lastName.value;

    let tdBirthDate = document.createElement('td');
    tdBirthDate.innerText = birthDate.value;

    let tdDelete = document.createElement('td');

    let btnDelete = document.createElement('button');
    btnDelete.innerText = "Delete user";

    tbody.appendChild(tr);
    tr.appendChild(tdFirstName);
    tr.appendChild(tdLastName);
    tr.appendChild(tdBirthDate);
    tr.appendChild(tdDelete);
    tdDelete.appendChild(btnDelete);

    btnDelete.addEventListener('click', event => {
        let object = event.target;
        let td = object.parentNode;
        let tr = td.parentNode;
        let tbody = tr.parentNode;
        tbody.removeChild(tr);
    });
});

// firstName.addEventListener('focusin', event => {
//     console.log(event);
//     event.target.style = "border: 2px solid #4CAF50";
// });
