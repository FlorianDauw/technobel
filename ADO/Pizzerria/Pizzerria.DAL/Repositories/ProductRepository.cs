﻿using Pizzerria.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ToolBox.ADO.Utils;

namespace Pizzerria.DAL.Repositories
{
    public class ProductRepository : BaseRepository<Product>
    {
        public ProductRepository(string connectionString, string providerName) : base(connectionString, providerName)
        {
        }
        public IEnumerable<Product> GetProductsByCategoryId(int id)
        {
            using (IDbConnection conn = GetConnection())
            {
                conn.Open();
                string query = "SELECT * FROM Product WHERE Id = @p1";
                Dictionary<string, object> dico = new Dictionary<string, object>
                {
                    { "@p1", id }
                };
                IDbCommand cmd = CreateCommand(conn, query, dico);
                IDataReader r = cmd.ExecuteReader();
                while (r.Read())
                {
                    yield return ReaderToEntityMapper(r);
                }
            }
        }
        public Product GetProductByName(string name)
        {
            using (IDbConnection conn = GetConnection())
            {
                conn.Open();
                string query = "SELECT * FROM Product WHERE Name = @p1";
                Dictionary<string, object> dico = new Dictionary<string, object>
                    {
                        { "@p1", name }
                    };
                IDbCommand cmd = CreateCommand(conn, query, dico);
                IDataReader r = cmd.ExecuteReader();
                if (r.Read())
                {
                    return ReaderToEntityMapper(r);
                }
                return null;
            }
        }
        public IEnumerable<Product> GetProductsWithPriceBetween(decimal min, decimal max)
        {
            using (IDbConnection conn = GetConnection())
            {
                conn.Open();
                string query = "SELECT * FROM Product WHERE Price BETWEEN @p1 AND @p2";
                Dictionary<string, object> dico = new Dictionary<string, object>
                {
                    { "@p1", min },
                    { "@p2", max }
                };
                IDbCommand cmd = CreateCommand(conn, query, dico);
                IDataReader r = cmd.ExecuteReader();
                while (r.Read())
                {
                    yield return ReaderToEntityMapper(r);
                }
            }
        }
    }
}
