﻿/*
Modèle de script de post-déploiement							
--------------------------------------------------------------------------------------
 Ce fichier contient des instructions SQL qui seront ajoutées au script de compilation.		
 Utilisez la syntaxe SQLCMD pour inclure un fichier dans le script de post-déploiement.			
 Exemple :      :r .\monfichier.sql								
 Utilisez la syntaxe SQLCMD pour référencer une variable dans le script de post-déploiement.		
 Exemple :      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

--Category
INSERT INTO [Category] VALUES('Pizzas');

--Product
INSERT INTO [Product] VALUES('4 saisons','Artichauds, jambon, olives, champignons', 'https://img.cuisineaz.com/400x320/2016-04-28/i1125-pizza-4-saisons.jpg', 10.50, 1);

--User
INSERT INTO [User] VALUES('Florian','Dauw', 'florian.dauw@hotmail.com', 'User', HASHBYTES('SHA2_512', 'test1234'));

--Order
INSERT INTO [Order] VALUES('REF0005', GETDATE(),'Payé', 1);

--OrderLine
INSERT INTO [OrderLine] VALUES(4, 10.50, 1, 1);