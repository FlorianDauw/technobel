let list = [
    "https://www.18h39.fr/wp-content/uploads/2019/04/chat-trop-chou-600x420.jpg",
    "https://img.freepik.com/photos-gratuite/rainette-verte-grenouille-decharnee-papouane-rainette-verte_146081-40.jpg?size=626&ext=jpg",
    "https://static.teteamodeler.com/media/cache/thumb_400/ecureuil-explication-tte-modeler-du-mot-ecureuil.png"
];

let index = 0;
let img = document.getElementById('main_pic');
let btn_left = document.getElementById('btn_left');
let btn_right = document.getElementById('btn_right');

btn_left.addEventListener('click', e => {
    index = ((index - 1) + list.length) % list.length;
    img.setAttribute('src', list[index]);
});

btn_right.addEventListener('click', e => {
    index = (index + 1) % list.length;
    img.setAttribute('src', list[index]);
});