let button = document.getElementById('btn-answer');
let answers = document.getElementsByTagName('span');
button.addEventListener('click', e => {
    for(let item of answers){
        item.classList.toggle('answer');
    }
    if(button.innerText === "Afficher les réponses"){
        button.innerText = 'Masquer les réponses';
    }
    else {
        button.innerText = 'Afficher les réponses';
    }
})