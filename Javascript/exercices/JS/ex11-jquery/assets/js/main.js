// let cpt = 1;

// $('#btnAdd').on('click', e => {
//     $('#tableBody').append(`
//         <tr>
//             <td>Item ${cpt} </td>
//             <td><button class="newButton">-</button></td>
//         </tr>
//     `);
//     cpt++;
// });

// $('body').on('click', '.newButton', e => {
//     let current = $(e.target);
//     console.log(current.parent('tr'));
    
//     current.parents('tr').remove();
//     cpt--;
// });


// //vérifie que la page soit bien chargée
// $(() => {

// });

// //vérifie que la page soit bien chargée (idem)
// $(document).ready(() => {

// });

let rowTemplate = `
    <tr>
        <td>__name__</td>
        <td>
            <button class="deleteBtn">X</button>
        </td>
    </tr>    
`

$(() => {
    let rowCount = 0;
    $('#btnAdd').on('click', e => {
        $('table tbody').append(rowTemplate
            .replace('__name__', 'item' + (++rowCount))
        );
    });

    $('tbody').on('click', 'deleteBtn', e => {
        $(e.target).parents('tr').remove();
        console.log($(e.target).parents('tr'));
        
    });
});

